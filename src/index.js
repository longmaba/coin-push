const request = require('request-promise');
const notifier = require('node-notifier');
const TelegramBot = require('node-telegram-bot-api');
const numeral = require('numeral');
const Discordie = require('discordie');
const Events = Discordie.Events;

const discordClient = new Discordie();

discordClient.connect({
  token: 'MzUyNDc4MzUyODc3MDI3MzMw.DIhvWQ.lMWEeI2RwqkjRxaTiMgqGVeyURc'
});

let channel;

discordClient.Dispatcher.on(Events.GATEWAY_READY, e => {
  console.log('Connected as: ' + discordClient.User.username);
  channel = discordClient.Channels.find(c => c.name == 'crypto-watcher');
});

const telegramToken = '408012058:AAFeHFGtIXreFfgmVNlS3Ac4cCYlSj1BIO4';

const bot = new TelegramBot(telegramToken);

const listWatcher = 'BTC ETH BCC XRP LTC IOTA NEM DASH NEO ETC XMR OMG QTUM STRAT ZEC EOS PAY LSK BTS SC CVC ADX NXT MCO'.split(
  ' '
);

const cryptoBotToken = '437034997:AAHSWxTHBtOVK97u6EwiPMtuoijhn17diVA';

const cryptoBot = new TelegramBot(cryptoBotToken, { polling: true });

const checkPrice = async () => {
  const data = await request('https://bittrex.com/api/v1.1/public/getmarketsummaries').json();
  let name = '';
  if (data.success) {
    let result = {};
    for (let i = 0; i < data.result.length; i++) {
      name = data.result[i].MarketName;
      if (name.substring(0, 3) == 'BTC' || name == 'USDT-BTC') {
        result[name] = {
          price: data.result[i].Last,
          vol: data.result[i].BaseVolume,
          buy: data.result[i].OpenBuyOrders,
          sell: data.result[i].OpenSellOrders,
          prev: data.result[i].PrevDay
        };
      }
    }
    return result;
  } else {
    return false;
  }
};

cryptoBot.onText(/\/btcvnd (.+)/, async (msg, match) => {
  const chatId = msg.chat.id;
  console.log(msg.from.first_name);
  const amount = match[1];
  const rate = await request('https://api.coindesk.com/v1/bpi/currentprice/VND.json').json();
  if (rate) {
    let string = numeral(rate.bpi.VND.rate_float * amount).format('0,0');
    let string2 = numeral(rate.bpi.USD.rate_float * amount).format('0,0');
    let message = `${msg.from.first_name}: ${amount} btc = ${string2} USD = ${string}đ`;
    cryptoBot.sendMessage(chatId, message);
  }
});

const chatId = -1001103408471;
let result = {};
const opts = {
  parse_mode: 'Markdown'
};

setInterval(
  async () => {
    try {
      let lastCheck = await checkPrice();
      if (lastCheck) {
        for (let key in lastCheck) {
          let old = result[key];
          let last = lastCheck[key];
          if (result[key] == null) {
            result[key] = lastCheck[key];
          } else {
            if (old != null && last != null) {
              if (last.price < old.price * 0.95 || last.price > old.price * 1.05) {
                let message = last.price > old.price ? '*PUMPING*:' : '*DUMPING*:';
                let percentage = (-(1 - last.price / old.price) * 100).toFixed(2);
                let change24h = (last.price / last.prev - 1) * 100;
                let caution = '';
                if (change24h > 20) {
                  caution = `*COIN PUMPING HARD* \n`;
                } else if (change24h < -20) {
                  caution = `*COIN DUMPING HARD* \n`;
                }
                if (last.vol > 50 || change24h > 20 || change24h < -20) {
                  change24h = change24h.toFixed(1);
                  let botMess = `${caution}${key} \n${message} From ${old.price} to ${last.price} (${percentage}%)\n*24h CHANGE*: ${change24h}% \n*VOL*: ${last.vol} \n*BO*: ${last.buy} / *SO*: ${last.sell} \nhttps://bittrex.com/Market/Index?MarketName=${key}`;
                  bot.sendMessage(chatId, botMess, opts);
                  if (channel != null) {
                    channel.sendMessage(botMess);
                  }
                }
                result[key] = lastCheck[key];
              }
            }
          }
        }
      }
    } catch (err) {
      console.log(err);
    }
  },
  1000
);
